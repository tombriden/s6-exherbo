# s6 policy for exherbo
PREFIX  ?= /etc
DESTDIR ?= /

all: install

install:
	mkdir -p "$(DESTDIR)/$(PREFIX)"
	cp -r --preserve=mode etc/* "$(DESTDIR)/$(PREFIX)"
